package main

import "fmt"

func main() {
	//aritmatika
	var result = 1 + 1
	fmt.Println(result)

	var a = 1
	var b = 3
	var c = a * b
	c += c
	c++
	fmt.Println(c)

	//perbandingan
	name1 := "yusron"
	name2 := "Yusron"

	var cek bool = name1 == name2
	fmt.Println(cek)

	angka1 := 1
	angka2 := 2
	fmt.Println(angka1 > angka2)
	fmt.Println(angka1 < angka2)
	fmt.Println(angka1 == angka2)
	fmt.Println(angka1 != angka2)

	//operasi boolean

}
