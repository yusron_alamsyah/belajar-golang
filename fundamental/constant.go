package main

import "fmt"

func main() {
	// const yaitu var yg tidak bisa diubah dan harus diset valuenya
	// const tidak error jika tidak digunakan
	const firstName string = "Yusron"
	const lastName = "Alamsyah"

	fmt.Println(firstName)
	fmt.Println(lastName)

	//multiple cons
	var (
		first = "Icus"
		last  = "Alamsyah"
	)
	fmt.Println(first)
	fmt.Println(last)

}
