package main

import "fmt"

func main() {
	name := "yusron alamsyah"

	switch name {
	case "yusron":
		fmt.Println("Hi yusron")
	case "alamsyah":
		fmt.Println("Hi alam")
	default:
		fmt.Println("Kenalan dong")
	}

	switch panjang := len(name); panjang > 5 {
	case true:
		fmt.Println("nama panjang")
	case false:
		fmt.Println("nama panjang")
	}

	p := len(name)
	switch {
	case p > 10:
		fmt.Println("nama terlalu panjang")
	case p > 5:
		fmt.Println("nama panjang")
	default:
		fmt.Println("nama benar")
	}
}
