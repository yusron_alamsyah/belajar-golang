package main

import "fmt"

func main() {

	name := "yusron"

	if name == "yusron" {
		fmt.Println("Hi yusron")
	} else if name == "alamsyah" {
		fmt.Println("Hi alam")
	} else {
		fmt.Println("Kenalan dong")
	}

	if panjang := len(name); panjang > 5 {
		fmt.Println("nama panjang")
	} else {
		fmt.Println("nama benar")
	}

}
