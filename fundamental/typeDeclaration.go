package main

import "fmt"

func main() {
	//mendeklarasikan type data ulang

	type noKtp string
	type isMarried bool

	var noKtpYusron noKtp = "1234567890"
	var isMarriedYusron isMarried = true
	fmt.Println(noKtpYusron)
	fmt.Println(isMarriedYusron)

}
