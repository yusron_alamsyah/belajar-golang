package main

import "fmt"

func main() {
	//Integer
	fmt.Println("-----Integer-----")
	fmt.Println("Satu = ", 1)
	fmt.Println("Tiga Koma 5 = ", 3.5)

	//Bool
	fmt.Println("-----Boolean-----")
	fmt.Println("Benar = ", true)
	fmt.Println("Salah = ", false)

	//String
	fmt.Println("-----String-----")
	fmt.Println("Create by Yusron")
	fmt.Println("len : ", len("Create by Yusron"))
	fmt.Println("get Char : ", "Yusron"[0]) //return byte

}
