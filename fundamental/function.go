package main

import "fmt"

// func with param
func sayHello(nama string) {
	fmt.Println("Hello ,", nama)
}

// func with return
func getHello(nama string) string {
	return "Hi " + nama
}

//funct with multiple return
func getFullName(nama string, umur int) (string, int, string) {
	return nama, umur, "hii"
}

//func with named return value
func getComplateName() (firstName, lastName string) {
	firstName = "Yusron"
	lastName = "Alamsyah"
	return
}

//funct variadic
func sumAll(nomer ...int) int {
	total := 0
	for _, v := range nomer {
		total += v
	}
	return total
}

func main() {
	fmt.Println("-----func with param-----")
	sayHello("yusron")

	fmt.Println("-----func with return-----")
	result := getHello("Icus Alamsyah")
	fmt.Println(result)

	fmt.Println("-----funct with multiple return-----")
	nama, umur, _ := getFullName("Yusron", 23)
	fmt.Println(nama)
	fmt.Println(umur)

	fmt.Println("-----func with named return value-----")
	a, b := getComplateName()
	fmt.Println(a, b)

	fmt.Println("-----func with variadic-----")
	//hanya bisa digunakan di param terakhir
	total := sumAll(1, 2, 3, 4, 5, 6)
	fmt.Println(total)

	sliceNomer := []int{10, 10, 10, 10, 10}
	total2 := sumAll(sliceNomer...)
	fmt.Println(total2)

}
