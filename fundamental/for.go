package main

import "fmt"

func main() {

	counter := 1
	for counter <= 5 {
		fmt.Println("counter = ", counter)
		counter++
	}

	for i := 1; i <= 5; i++ {
		fmt.Println("i= ", i)
	}

	slice := []string{"yusron", "alamsyah"}
	for _, v := range slice {
		fmt.Println(v)
	}

	//break & continue

	fmt.Println("-----Break-----")
	for i := 1; i <= 5; i++ {

		fmt.Println("i= ", i)
		if i == 3 {
			break
		}
	}
	fmt.Println("-----Continue-----")
	for i := 1; i <= 5; i++ {
		if i%2 == 0 {
			continue
		}
		fmt.Println("i= ", i)

	}

}
