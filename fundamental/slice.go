package main

import "fmt"

func main() {
	//slice merupakan potongan array
	//slice mirip array cuman ukuran slice bisa berubah
	//tipe data slice memiliki 3 data yaitu pointer , lenght,capacity

	//pointer : petunjuk data pertama di array pada slice
	//lengt : panjang
	//capacity : kapasitas slice , lengt tidak boleh kurang dari capacity

	var bulan = [...]string{
		"jan",
		"feb",
		"mar",
		"apr",
		"mei",
		"jun",
		"jul",
		"aug",
		"sep",
		"okt",
		"nov",
		"des",
	}

	var slice1 = bulan[4:7]
	fmt.Println(slice1)
	fmt.Println(len(slice1))
	fmt.Println(cap(slice1))

	newSlice := make([]string, 2, 5)

	newSlice[0] = "isi slice 0"
	newSlice[1] = "isi slice 1"

	fmt.Println(newSlice)

	iniArray := [3]int{1, 2, 3}
	iniSLice := []int{1, 2, 3}
	fmt.Println(iniArray)
	fmt.Println(iniSLice)
}
