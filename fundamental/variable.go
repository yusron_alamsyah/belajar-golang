package main

import "fmt"

func main() {
	var name string //harus mendeklarasi type data kalau tidak ada set value
	name = "Yusron"
	fmt.Println(name)

	var age = 23 //tidak harus mendeklarasi type data kalau  ada set value
	fmt.Println(age)

	hobby := "futsal" // var tidak perlu jika ada value dan set menggunakan := (deklarasi awal)
	fmt.Println(hobby)

	var (
		firstName = "Icus"
		lastName  = "Alamsyah"
	) //multiple var
	fmt.Println(firstName)
	fmt.Println(lastName)

}
