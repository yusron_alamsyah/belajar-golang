package main

import "fmt"

func main() {

	person := map[string]string{
		"nama":   "yusron",
		"alamat": "malang",
	}
	person["jk"] = "pria"
	delete(person, "jk")
	fmt.Println(person)
	fmt.Println(person["nama"])

	var book map[string]string = make(map[string]string)

	book["judul"] = "ini judul"
	fmt.Println(book)

}
