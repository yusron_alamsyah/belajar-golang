package main

import "fmt"

func main() {
	//array kumpulan data dengan tipe yg sama
	//ketika membuat array harus menentukan jumlah lenght array
	//tidak bisa menambah array

	var names [2]string
	names[0] = "yusron"
	names[1] = "alamsyah"

	fmt.Println(names[0])
	fmt.Println(names[1])

	values := [2]int{
		4,
		7,
	}
	fmt.Println(values)

	fmt.Println(len(values))

}
